<?php

namespace Drupal\maintenance_ip_whitelist;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class for reacting to form operations.
 *
 * @internal
 */
class FormOperations implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new FormOperations instance.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The State service.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state')
    );
  }

  /**
   * Alters forms to configure a list of authorized IPs.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $form_id
   *   The form ID.
   *
   * @see hook_form_alter()
   */
  public function formAlter(array &$form, FormStateInterface $form_state, $form_id) {
    if ('system_site_maintenance_mode' === $form_id) {
      $form['maintenance_ip_whitelist'] = [
        '#type' => 'textarea',
        '#title' => $this->t('IP whitelist'),
        '#description' => $this->t('The IP adresses listed in this field will be able to access the site in maintenance even as anonymous users. Enter one value per line.'),
        '#default_value' => $this->state->get('maintenance_ip_whitelist', ''),
        '#weight' => 99,
      ];
      $form['#submit'][] = [$this, 'submitForm'];
    }
  }

  /**
   * Form submit handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array $form, FormStateInterface $form_state) {
    $whitelist = $form_state->getValue('maintenance_ip_whitelist');
    $this->state->set('maintenance_ip_whitelist', $whitelist);
  }

}
