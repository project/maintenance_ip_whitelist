<?php

namespace Drupal\maintenance_ip_whitelist;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Site\MaintenanceModeInterface;
use Drupal\Core\State\StateInterface;

/**
 * Provides the default implementation of the maintenance mode service.
 */
class MaintenanceModeDecorator implements MaintenanceModeInterface {

  /**
   * The decorated MaintenanceMode service.
   *
   * @var \Drupal\Core\Site\MaintenanceModeInterface
   */
  protected MaintenanceModeInterface $decorated;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * Constructs a new maintenance mode service.
   *
   * @param \Drupal\Core\Site\MaintenanceModeInterface $decorated
   *   The discovery object that is being decorated.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state.
   */
  public function __construct(MaintenanceModeInterface $decorated, StateInterface $state) {
    $this->decorated = $decorated;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    return $this->decorated->applies($route_match);
  }

  /**
   * {@inheritdoc}
   */
  public function exempt(AccountInterface $account) {
    // Retrieve the IP whitelist and exempt all concerned users.
    $whitelist = $this->state->get('maintenance_ip_whitelist', '');
    $whitelist = array_filter(array_map('trim', explode("\n", $whitelist)));

    if (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
      return TRUE;
    }

    // Let the core check all other users.
    return $this->decorated->exempt($account);
  }

  /**
   * {@inheritdoc}
   */
  public function getSiteMaintenanceMessage() {
    return $this->decorated->getSiteMaintenanceMessage();
  }

}
